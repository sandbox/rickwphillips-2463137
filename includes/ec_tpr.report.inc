<?php
/*
 * @file
 * File to display an individual TPR submission
 */
 
 
 
 /*
  * Function to display at TPR submission. 
  */
function view_ec_tpr($sid) {
  
  if(is_numeric($sid)) {
    
    drupal_add_css(drupal_get_path('module', 'ec_tpr') . '/css/ec_tpr_styles.css', 'file');
    
    $rows = array();
    $header = array();
    /*
    $connection = Database::getConnection('default', 'edbd');
   
    try {
      $TPS = $connection->query('SELECT *   FROM TPS where sid = :sid', array(':sid' => $sid))
        ->fetchAssoc();
    }
    catch(PDOexception $e) {
      drupal_set_message('131313 Submission View Error ' . $e->getMessage(), 'warning');
    }*/
    
    $TPS = entity_load('ec_tpr', array($sid));
    
    
    
    if($TPS) {
      
      $TPS = array($TPS[$sid]);
      
      
      $fields = _form_fields_list();
         
      
      foreach($fields as $titles => $section) {
        $items = array();
        $cols = 0;
        
       $rows[][] = array(
        'data' =>  $titles,
        'colspan' => 6,
        'class' => 'tps-heading',
      );
      
      foreach($section as $name => $cell) {
        
        #$TPS = $TPS[0];
        
        #$TPS = $TPS[0];
        $data = preg_replace('/_/', ' ', $TPS[0]->$name);
        $data = preg_replace('/\(.*\]/', '', $data);
        
        if($TPS[0]->$name == '' && $name != 'filler_cell') $TPS[0]->$name = '-';
        $colon = ($name == 'filler_cell') ? '' : ':';
       
        $items[] = array(
            'data' => strtoupper($cell['label']) . $colon,
            'class' => $cell['class'] . 'tps-label ',
          );
          
         $items[] = array(
            'data' => $data,
            'colspan' => $cell['colspan'],
            'class' => $cell['class'] . 'tps-value ',
          );
        
        $cols += $cell['colspan'] + 1;
        
        

        
        if($cols >= 6) {
          
          $rows[] = $items;
          $items = array();
          $cols = 0;  
        }
       } // end of foreach
       
       if($items) {
         $items[] = array(
          'data' => '',
          'colspan' => 6 - ($items[ (count($items) - 1) ]['colspan'] - 1),
          'class' => 'tps-spacer ',
          );
         $rows[] = $items;
         $items = array();
       }
       
      }
      
      
      
      $testFields = _form_fields_list($TPS, 'tests');
      
      $rows[] = array(
      array(
          'data' => '',
          'colspan' => 1,
          'class' => 'tps-heading ',
        ),
        array(
          'data' => 'Emuge',
          'colspan' => 2,
          'class' => 'tps-heading ',
        ),
        array(
          'data' => 'Competitor',
          'colspan' => 2,
          'class' => 'tps-heading ',
        ),
        array(
          'data' => '',
          'colspan' => 1,
          'class' => 'tps-heading ',
        ),
      );

      $rows[] = array(
      array(
          'data' => '',
          'colspan' => 1,
          'class' => 'test-label ',
        ),
        array(
          'data' => 'Emuge',
          'colspan' => 2,
          'class' => 'emuge tps-value',
        ),
        array(
          'data' => $testFields['Comp']['comp_manufacturer']['data'],
          'colspan' => 2,
          'class' => 'comp tps-value',
        ),
      );
  
      foreach(array('init_fields', 'Parts Data', 'Time Data', 'Size Data', 'Cost Data') as $toolData){
  
        if($toolData != 'init_fields') {
          $rows[] = array(
            array(
              'data' => '',
              'colspan' => 1,
              'class' => 'test-label ',
            ),
            array(
              'data' => $toolData,
              'colspan' => 4,
              'class' => 'tps-heading tps-data',
            ),
            
          );  
        }
        
        foreach($testFields[$toolData] as $key) {
          
          if(startsWith($key, '_')) {
            $emuge = array('emuge', '');
            $comp = array('comp', '' );
            $i = 0;
            $r = 0;
          }
          else {
            $emuge = array('', 'test results');
            $comp = array('', 'test results' );
            $i = 1;
            $r = 0;
           
          }
          
          $emugeData = ucfirst($testFields[ucfirst($emuge[$i])][$emuge[$r] . $key]['data']);
          $compData = ucfirst($testFields[ucfirst($comp[$i])][$comp[$r]. $key]['data']);
          
          if(in_array($emugeData, array('', NULL))) $emugeData = '-';
          if(in_array($compData, array('', NULL))) $compData = '-';
          
          $rows[] = array(
        array(
            'data' => label_prep($testFields[0]['test_labels']->$emuge[$r] . $key) . ':',
            'colspan' => 1,
            'class' => 'test-label ',
          ),
          array(
            'data' => $emugeData,
            'colspan' => 2,
            'class' => 'emuge tps-value',
          ),
          array(
            'data' => $compData,
            'colspan' => 2,
            'class' => 'comp tps-value',
          ),
        );
        
        } // end of foreach $testFields['init_fields']
      } // end of foreach array() as $toolData
      
       $rows[] = array(
            array(
              'data' => '',
              'colspan' => 1,
              'class' => 'test-label ',
            ),
            array(
              'data' => 'Test Results',
              'colspan' => 2,
              'class' => 'tps-heading results-heading',
            ),
            array(
              'data' => '',
              'colspan' => 2,
              'class' => 'tps-heading results-heading',
            ),
            
            
          );  
          
      

      
      $ResultsClass = strtolower($testFields['Test results']['success']['data']);
      if($ResultsClass == 'fail') {
          $testFields['Test results']['job_savings'] = preg_replace('/\$-/', '-$', $testFields['Test results']['job_savings']);
          $testFields['Test results']['part_savings'] = preg_replace('/\$-/', '-$', $testFields['Test results']['part_savings']);
      }
      
      foreach($testFields['Test Results'] as $key) {
        
        if($key == 'job_savings_percent') {
          $eCost = preg_replace('/\$|\,/', '', $TPS['emuge_total_cost']);
          $cCost = preg_replace('/\$|\,/', '', $TPS['comp_total_cost']);
          $percent = number_format((1 - ($eCost / $cCost)) * 100, 1);
          $testFields['Test results'][$key]['data'] = $percent . '%';
        }
        
        
        
        $rows[] = array(
          array(
              'data' => $testFields['test_labels'][$key],
              'colspan' => 1,
              'class' => 'test-label tps-label ' . $ResultsClass,
            ),
            array(
              'data' => $testFields['Test results'][$key]['data'],
              'colspan' => 2,
              'class' => 'total results tps-value ' . $ResultsClass,
            ),
             array(
              'data' => '',
              'colspan' => 2,
              'class' => 'total results tps-value ' . $ResultsClass,
            ),
        );
      }
      
    } // end of if($TPS)
    else {
      drupal_set_message('Report #' . $sid, 'error');
      $rows[] = array('No - Report #' . $sid . ' does not exists.');
    }
  } 
  else {
   drupal_goto($base_root . 'tpr');
  }
  
  #$rows[] = array('1', '2', '3', '4', '5');
  
  return $page['#markup'] = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('width' => '100%')));
  
}

/*
 * Function to provide the appropriate fields and labels to the TPR submission form
 * 
 * @param
 *  $TPS
 * @param
 *  $layout
 * @param
 *  $tests
 *
 * @return $fields
 */
function _form_fields_list(&$TPS = array(), $layout = 'page', $tests = array()) {
  
  switch($layout) {
  
    case 'page':

      $fields = array();
      
      $fields['Tool Performance Report Data'] = array(
        'report_date' => array(
          'label' => 'Report Date',
          'colspan' => 1,
          'class' => '',
        ),
        'sales_area' => array(
          'label' => 'Country',
          'colspan' => 1,
          'class' => '',
        ),
        'sales_manager' => array(
          'label' => 'Sales Manager',
          'colspan' => 1,
          'class' => '',
        ),
        'dist' => array(
          'label' => 'Distributor',
          'colspan' => 1,
          'class' => '',
        ),
        'dist_contact' => array(
          'label' => 'Contact',
          'colspan' => 1,
          'class' => '',
        ),
        'filler_cell' => array(
          'label' => '',
          'colspan' => 1,
          'class' => 'tps-spacer ',
        ),
        'end_user' => array(
          'label' => 'End User',
          'colspan' => 1,
          'class' => '',
        ),
        'end_user_contact' => array(
          'label' => 'Contact',
          'colspan' => 1,
          'class' => '',
        ),
      
      );
      
      $fields['Tool Performance Report Tool Data'] = array(
        'size' => array(
          'label' => 'Size',
          'colspan' => 1
        ),
        'tool_type' => array(
          'label' => 'Tool Type',
          'colspan' => 1
        ),
        'class_fit' => array(
          'label' => 'Class of Fit',
          'colspan' => 1
        ),
        'tool_brand' => array(
          'label' => 'Tool Brand',
          'colspan' => 1
        ),
        'machining_plane' => array(
          'label' => 'Machining Plane',
          'colspan' => 1
        ),
        'machine_tool_condition' => array(
          'label' => 'Machine Condition',
          'colspan' => 1
        ),
        'workpiece_desc' => array(
          'label' => 'Workpiece Type',
          'colspan' => 1
        ),
        'workpiece_hardness' => array(
          'label' => 'Hardness',
          'colspan' => 1
        ),
        'material' => array(
          'label' => 'Material',
          'colspan' => 1
        ),
        'holder_condition' => array(
          'label' => 'Holder Condition',
          'colspan' => 1
        ),
        'coolant_lube' => array(
          'label' => 'Coolant/Lube',
          'colspan' => 1
        ),
        'submaterial' => array(
          'label' => 'Sub-Material',
          'colspan' => 1
        ),
        'holder_type' => array(
          'label' => 'Holder Type',
          'colspan' => 1
        ),
        'coolant_psi' => array(
          'label' => 'Coolant PSI',
          'colspan' => 1
        ),
        'iso_material_group' => array(
          'label' => 'ISO Group',
          'colspan' => 1
        ),
      );   
      
    return $fields;      
    
    foreach(array('Emuge', 'Comp', 'Test Results', 'Emuge Costs', 'Comp Costs' ) as $key) {   
    
     ksort($fields[$key]);
    }
    break;
    
    case 'tests' :
      
      

      foreach($TPS[0] as $key => $value) {
        
        if(in_array($TPS[0]->$key, array('', NULL)) || !isset($TPS[0]->$key)) $TPS[0]->$key = '-';
               
        if(startsWith($key, 'emuge') && !in_array($value, array('', NULL))) {
            if($TPS[0]->emuge_application != 'drilling' && $key == 'emuge_regrind_total_cost') {
            }
            elseif(endsWith($key, 'cost') || endsWith($key, 'price')) {
              $TPS[0]->$key = '$' .  number_format($value, 2);
              if(endsWith($key, 'total_cost')) {
                $class = 'total ';
              }
              else {
                $class = ''; 
              }
              
              $fields['Emuge'][$key] = array(
                'data' => $TPS[0]->$key,
                'colspan' => 2,
                'label' => $key,
                'class' => $class . 'emuge-costs ',
              );
            }
            else {
               $fields['Emuge'][$key] = array(
                'data' => $value,
                'colspan' => 2,
                'label' => $key,
                'class' => 'emuge ',   
                );
            }
        }
        
        else if(startsWith($key, 'comp') && !in_array($value, array('', NULL))) {
          
            if($TPS[0]->comp_application != 'drilling' && $key == 'comp_regrind_total_cost') {
            }
            elseif(endsWith($key, 'cost') || endsWith($key, 'price')) {
              if(endsWith($key, 'total_cost')) {
                $class = 'total ';
              }
              else {
                $class = ''; 
              }
              
              $TPS[0]->$key = '$' .  number_format($value, 2);
              $fields['Comp'][$key] = array(
                'data' => $TPS[0]->$key,
                'colspan' => 2,
                'label' => $key,
                'class' => $class . 'comp-costs ',
              );
            }
            else {
               $fields['Comp'][$key] = array(
                'data' => $value,
                'colspan' => 2,
                'label' => $key,
                'class' => 'comp ', 
            );
            }
      }
      
        else if(in_array($key, array('success', 'job_savings', 'part_savings', 'hourly_rate'))) {
        if(endsWith($key, 'savings') || endsWith($key, 'rate') ) {
          if($key == 'job_savings') {
            $color = ($TPS[0]->$key > 0) ? 'success ' : 'fail ';
          }
          else {
            $color = '';
          }
          
          $TPS[0]->$key = '$' . number_format($TPS[0]->$key, 2);
          
          $fields['Test results'][$key] = array(
            'data' => $TPS[0]->$key,
            'colspan' => 5,
            'label' => $key,
            'class' => $color . 'results ',
          );
        } 
        else {
          $fields['Test results'][$key] = array(
            'data' => $TPS[0]->$key,
            'colspan' => 5,
            'label' => $key,
            'class' => 'test results '
          );
          
        }
      } 
      else {
        $fields['Test results'][$key] = array(
            'data' => $TPS[0]->$key,
            'colspan' => 2,
            'label' => $key,
            'class' => $color . 'results ',
        );
      } 
      
      
    }
   
   #$connection = Database::getConnection('default', 'edbd');
   
   $labels = entity_load('ec_tpr', array('sid' => 1919));
   
  /*try{
    $labels = $connection->query('SELECT * FROM TPS WHERE sid = 1919')->fetchAssoc();
  }
  catch(PDOexception $e){
    drupal_set_message($e->getMessage(), 'error');
  }*/
  $fields['test_labels'] = array($labels[1919]);
  
  foreach($fields['test_labels'][0] as $field => $label) {
    
    
    if(endsWith($label, '(in)')) {
      
      
    
    if(startsWith($field, 'emuge')) {
      $co = 'Emuge';
    }
    elseif(startsWith($field, 'comp')) {
      $co = 'Comp';
    }
    else {
      $co = '';
    }
    
    
    
    $fields[$co][$field]['data'] = number_format($fields[$co][$field]['data'], 4);
      
      
    }  
  }
  $_fields['_init_fields'] = array('_application', '_part_num', '_part_price',  'holes_part', 'parts_job', 'holes_job', '_holes', '_tools_job', '_cutting_speed' ,  '_cycle_time_ph' ,  'change_time', '_total_change_time', '_mach_time_hr', '_spindle', '_IPM', '_IPR', '_thread_depth', '_nom_dia', '_drill_size_in', '_pitch_val',
   'hourly_rate', '_tool_cost',
  '_mach_cost',  '_change_cost', '_total_cost');
  $fields['init_fields'] = array('_application', '_part_num', '_part_price',);
  $fields['Parts Data'] = array('holes_part', 'parts_job', 'holes_job', '_holes', '_tools_job');
  $fields['Time Data'] = array('_cutting_speed', '_spindle',  '_cycle_time_ph' ,  'change_time', '_total_change_time', '_mach_time_hr', );
  $fields['Size Data'] = array( '_IPM', '_IPR', '_thread_depth', '_drill_size_in', );
  $fields['Cost Data'] = array( 'hourly_rate', '_tool_cost',
  '_mach_cost',  '_change_cost');
  
  if($TPS['emuge_application'] != 'Drilling') {
    
      array_push($fields['Size Data'], '_nom_dia', '_pitch_val');
      
      
      
      if(!in_array($TPS['comp_application'], array('Tapping', 'tapping', '-', '', NULL)) || !in_array($TPS['emuge_application'], array('Tapping', 'tapping'))) {
        
         array_push($fields['Parts Data'], '_thread_number', '_flutes', '_passes'); 
         array_push($fields['Time Data'],  '_ipm_center');
      }
  }
  else {
      array_push($fields['Parts Data'], '_regrinds_tool', '_regrinds_job');
      array_push($fields['Cost Data'], '_regrind_tool_cost', '_regrind_total_cost' );
  }
  
  array_push($fields['Cost Data'], '_total_cost');
  
  $fields['Test Results'] = array('job_savings', 'part_savings', 'job_savings_percent', 'success');
  /*
  $fields['Tap Information'] = array('_pitch_val',);
  $fields['Thread Mill Information'] = array('_pitch_val', '_thread_number', 'ipm_center', '_flutes', '_passes');
  $fields['Drill Information'] = array( '_regrind_job', '_regrind_tool_cost', '_regrind_total_cost');
  */
  break;
    
  default:
  }
  
  return $fields;

}

/*
 * Functions to determine if a variable starts or ends with a $needle
 */
 
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

/*
 * Function to preparte the TPR Results data labels.
 */
function label_prep($label = '') {

  $label = preg_replace('/_/', ' ', $label);
  
  return strtoupper($label);  
}